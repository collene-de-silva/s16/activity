let number = parseInt(prompt("Please enter a number"));
console.log("The number you provided is " + number + ".")


for (let i = number; i > 0; i--) {

	if (i % 10 === 0) {
		console.log("The number is divisible by 10. Skipping the number.")
		continue
	}

	if (i % 5 === 0) {
		console.log(i)
		continue
	}

	if (i <=50) {
		console.log("The current value is at 50. Terminating the loop.")
		break
	}
}



//STRETCH GOALS
let weirdWord = "supercalifragilisticexpialidocious"
const consonants = []


for (let x = 0; x < weirdWord.length; x++) {
	if (weirdWord[x] === "a" || weirdWord[x] === "e" || weirdWord[x] === "i" || weirdWord[x] === "o" || weirdWord[x] === "u"){
		continue
	} else {
		consonants.push(weirdWord[x])
		continue
	}
}

console.log(consonants)